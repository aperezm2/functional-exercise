/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */
const fderive = (fn, h) => {
  return x => {
    return (fn(x+h)-fn(x-h))/(2*h)
  }
}

const squear = Math.sqrt

module.exports = {
  fderive,
  squear,
};

const derivefn = fderive(squear, 0.001);
console.log(derivefn(6));
