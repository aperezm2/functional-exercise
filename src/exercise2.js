const count = numbers => {
  const counter = (accumulator) => accumulator + 1
  return numbers.reduce(counter)
}

const sum = numbers => {
  const reducer = (accumulator, val) => accumulator + val
  return numbers.reduce(reducer)
}

const faverage = numbers => {
  if (count(numbers) == 0)
    return 0
  return sum(numbers)/count(numbers)
};

module.exports = faverage;